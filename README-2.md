# Pre-requisites for Linux/Unix  

In order to work on this project, all the members are required to have a grip on the basics of Linux terminal commands.  

Following are few of the most frequently accessed **Linux/Unix terminal commands** and **directories**. The idea is to make everyone well aware of them with a brief description.  

_Describe the significance of the following (commands as well as directories) in a short yet optimal manner!_  

## Terminal Commands  

* `ls`:  <!-- Replace this block (from < to >) with your description -->  
* `ll`: <!-- Replace this block with your description -->  
* `umask`: <!-- Replace this block with your description -->  
* `cchmod`: <!-- Replace this block with your description -->  
* `chmod`: <!-- Replace this block with your description -->  
* `chown`: <!-- Replace this block with your description -->  
* `chgrp`: <!-- Replace this block with your description -->  
* `mount`: <!-- Replace this block with your description -->  
* `umount`: <!-- Replace this block with your description -->  
* `strace`: <!-- Replace this block with your description -->  
* `rm`: <!-- Replace this block with your description -->  
* `ps`: <!-- Replace this block with your description -->  
* `vmstat`: <!-- Replace this block with your description -->  
* `ifconfig`: <!-- Replace this block with your description -->  
* `ip`: <!-- Replace this block with your description -->  
* `who`: <!-- Replace this block with your description -->  
* `which`: <!-- Replace this block with your description -->  
* `finger`: <!-- Replace this block with your description -->  
* `ssh`: <!-- Replace this block with your description -->  
* `ftp`: <!-- Replace this block with your description -->  
* `scp`: <!-- Replace this block with your description -->  
* `ce`: <!-- Replace this block with your description -->  
* `pwd`: <!-- Replace this block with your description -->  
* `cd`: <!-- Replace this block with your description -->  
* `pwd`: <!-- Replace this block with your description -->  
* `grep`: <!-- Replace this block with your description -->  
* `sed`: <!-- Replace this block with your description -->  
* `awk`: <!-- Replace this block with your description -->  
* `nohup`: <!-- Replace this block with your description -->  
* `fg`: <!-- Replace this block with your description -->  
* `bg`: <!-- Replace this block with your description -->  
* `touch`: <!-- Replace this block with your description -->  
* `kill`: <!-- Replace this block with your description -->  
* `tail`: <!-- Replace this block with your description -->  
* `head`: <!-- Replace this block with your description -->  
* `top`: <!-- Replace this block with your description -->  
* `vi` and `vim`: <!-- Replace this block with a COMMON description -->  
    1. `vi`: <!-- Replace this block with your description -->  
    2. `vim`: <!-- Replace this block with your description -->  
* `cat`: <!-- Replace this block with your description -->  
* `man`: <!-- Replace this block with your description -->  
* `ln`: <!-- Replace this block with your description -->  
* `useradd`, `usermod` and `userdel`: <!-- Replace this block with a COMMON description -->  
    1. `useradd`: <!-- Replace this block with your description -->  
    2. `usermod`: <!-- Replace this block with your description -->  
    3. `userdel`: <!-- Replace this block with your description -->  
* `groupadd` and `groupdel`: <!-- Replace this block with a COMMON description -->  
    1. `groupadd`: <!-- Replace this block with your description -->  
    2. `groupdel`: <!-- Replace this block with your description -->  
* `systemctl`: <!-- Replace this block with your description -->  
* `init`: <!-- Replace this block with your description -->  
* `reboot`: <!-- Replace this block with your description -->  
* `restart`: <!-- Replace this block with your description -->  
* `screen`: <!-- Replace this block with your description -->  

## Directories  

* `/usr/bin` and `/usr/local/bin`: <!-- Replace this block with a COMMON description -->  
    1. `/usr/bin`: <!-- Replace this block with your description -->  
    2. `/usr/local/bin`: <!-- Replace this block with your description -->  
* `/etc`: <!-- Replace this block with your description -->  
* `/var/logs`: <!-- Replace this block with your description -->  
